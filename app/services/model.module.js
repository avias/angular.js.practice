(function (){
    'use strict';

    angular.module('app')
        .value('model', {
            "user" : "Andriy",
            "userPhoto" : "images/star.jpeg"
            // ,
            // "items" : [
            //    {"action" : "Estimate", "done" : false},
            //    {"action" : "Create", "done" : false},
            //    {"action" : "Edit...", "done" : true},
            //    {"action" : "Delete...", "done" : false} 
            // ]
        });
})();